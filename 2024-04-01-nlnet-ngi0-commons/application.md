# Application 2024-04-364

The following submission was recorded by NLnet. Thanks for your application, we look forward to learning more about your proposed project.

## Contact

- name: The Forgejo project
- phone: [redacted]
- email: funding.ngi0-common-funds-2024@forgejo.org
- organisation name: Codeberg e.V.
- country: Germany
- consent: You may keep my data on record

## Project

- code: 2024-04-364
- project name: Forgejo - Beyond coding. We forge.
- fund: Commons_Fund
- requested amount: € 50000
- website: https://forgejo.org/

## synopsis

Forgejo is a software forge designed to scale to millions of users and projects. It aims at being lightweight on resources and easy to self-host, without compromising on the ability to scale to large instances, like Codeberg.

Codeberg is a fast growing (8%+ per month) software forge running Forgejo and hosting ~125,000 Free Software projects and ~90,000 contributors.

Since its inception late 2022, the number of Forgejo contributors and users grew significantly. Quality releases were produced with a strong focus on security and stability. This growth is met with new requirements:

* User eXperience: fix the interactions that hinder the user's ability to perform a task and improve accessibility of lesser-known features.
* UI and accessibility: improve the semantic structure of the UI make it more consistent and accessibility.
* Development: tests that complete faster than 25 minutes, backport automation to stable releases, tools for dependency management to match the need of the contributors.
* Reliability: additional end to end testing and pull request synchronization across repositories to improve the robustness of stable releases. Improve the self-healing capabilities.
* Localization: owned since early 2024 it needs code adjustements and proper tooling to improve the translation quality and support for additional languages.

## experience

@oliverpool is a seasoned Go developer, eager to enable interoperability between platforms. He has contributed a number of pull requests to Forgejo (2 of them in the course of the NLNet grant 2022-12-035), some of them related to testing improvements (to catch missing translations for instance).
https://codeberg.org/forgejo/forgejo/pulls?state=closed&poster=32636

@algernon has contributed numerous fixes and features to Forgejo - with ample amount of tests. Which is why he wants them to run faster, too.
https://codeberg.org/forgejo/forgejo/pulls?state=closed&poster=57189

@caesar is a seasoned programmer and web developer who contributed to Forgejo since its inception, in the codebase as well as the website and the documentation.
https://codeberg.org/forgejo/forgejo/pulls?state=closed&poster=23304

Codeberg e.V. is a non-profit organization created in 2019, based in Berlin, Germany. It is dedicated to supporting Codeberg, a collaboration platform and Git hosting for Free Software, content and projects where a community of like-minded developers and content creators work together. Over the years Codeberg members developed numerous improvements to the codebase, for customization and, as it grew to become the largest public instance. These changes are currently available in a modified version of Forgejo dedicated to Codeberg and unsuitable for self-hosting or packaging in another context. Forgejo was a natural evolution along this path. In 2023 and 2024 Codeberg e.V. dedicated a large part of its buget to hire a Forgejo developer. Codeberg e.V. is in the best possible position to recruit and hire more skilled people to advance Forgejo and provide them with a productive environment.

## usage

The budget will be used exclusively to pay for the time of the people working on Forgejo at a 60€ per hour rate.

An NLNet Entrust Grant is ongoing (2022-12-035) and will be completed before this one starts.

Codeberg e.V. has funds dedicated to Forgejo.

Companies, institutions or individuals occasionally provide funding and sponsor specific pull requests.

## comparison

Forgejo is a fork (https://forgejo.org/2024-02-forking-forward/) of Gitea, which relies on proprietary platforms and proposes a non Open-Source version of the software forge. Forgejo is developed, tested, released and translated using Free Software.

## challenges

During the project, we expect to address the following testing challenges:
- run thousands of tests in continuous integration in an acceptable duration
- make tests self-contained and independant from each other
- reduce the friction to add new tests to the codebase and drastically ease the reproduction of bugs encountered by end-users

## ecosystem

Forgejo is becoming more and more widespread and deployed with large instances (e.g. Codeberg.org, git.disroot.org) as well as a hundreds of smaller instances.

The "relevant actors" are:
* System administrators who self-host a software forge. They expect reliable releases (having to upgrade multiple times in a short time window, due to bugfixes is not acceptable). They are made aware of the outcome by the release notes (and by the stability of Forgejo).
* Software forge users. They expect the advertized feature to reliably work, even after an upgrade.
* Forgejo contributors. They are expected to add tests to all of their contributions and will greatly benefit from an easy, fast and extendable testing suite.