# You requested 50000 euro. Can you provide some more detail on how you arrived at this amount? Could you provide a breakdown of the main tasks, and the associated effort? What would be the concrete deliverables within the scope of this project?

The requested amount would cover salaries to finance the following set of tasks. The costs associated with every task are added as percentages. 

Release process automation: a tool for automating release notes - the existing tools for generating release notes that is widely used in Free Software (conventional commits) is based on commit titles and does not take into account stable releases. Forgejo needs similar tooling where the granularity is the pull request and cope with multiple stable branches in addition to the development branch. (10%)

Forgejo for Kubernetes: a helm chart for Forgejo - in addition to container images and standalone binaries, kubernetes must be officially supported. The chosen packaging method is a helm chart. (10%)

Testing: end to end tools for federation - end to end tests for Forgejo are extended to be able to spawn multiple instances of Forgejo or other forges (GitLab etc.) and ActivityPub compliant software (GTS, Mastodon) to provide an environment where federation features scenario can be implemented. (5%); continuous deployment pipeline for stable and development releases - when a bug is reported for a stable release of Forgejo, it needs to be conveniently reproduced on the development branch as well as the earliest stable release. The tip of each branch is deployed daily and made publicly available as a set of open registration instance. (5%)

Continuous Delivery: tool for instant deployment - it allows anonymous and security deployment when a commit is pushed to Forgejo, by watching the logs and guarding against abuse from malicious actors. (5%)

Forgejo Runner Security: security release of the Forgejo Runner - based on the results of the ROS audit of the Forgejo runner, publish versions that fix security issues based on their recommendations. (15%)

Dependency management: automate dependency management - develop a Renovate configuration to monitor and upgrade Forgejo dependencies (over 300) in an automated way, for both the development branch and all stable branches. An upgrade policy is documented. It allows for a timely upgrade while reducing the burden for each Forgejo contributor to a minimum. (10%)

User eXperience: publish a User eXperience design process - Forgejo engaged in User Research in 2024, with numerous interviews to find fact based evidence to prioritize the features and issues that need to be addressed in a user centric way. This lead to a more rational approach to design features and improve Forgejo User eXperience that needs to be documented and put to use. (20%)

Localization: internationalization tooling - Forgejo relies on Weblate since early 2024 and it requires specific tooling that did not exist before, when the translations originated from another Git repository. (10%); document the localization process - The communication of translators that Forgejo built from the ground up since January 2024 relies on a documentation that needs to be improved. In its original version there were ambiguities and missing topics that created unnecessary friction when onboarding new volunteers. (10%)

# Could you describe the current relation between Forgejo and Gitea? 

The relation is by observing the issue tracker and the Free Software codebase. The proprietary codebase was released more recently (see https://gitea.com/commitgo/gitea-ee/releases compared to https://github.com/go-gitea/gitea/tags) but the source is not available, contrary to what GitLab does.

Forgejo is organized to examine all changes happening in Gitea on a weekly basis and has not failed to do so since the beginning. Gitea does something similar, as evidenced by a number of changes landing in Gitea after being implemented in Forgejo.

CommitGo and Gitea Ltd. - the two companies in control of Gitea - never agreed to communicate with Forgejo since 2022. There were two isolated occurrences late 2023 when an executive of the company however posted comments on the Gitea requirement for copyright assignment, in the Forgejo issue tracker. The same executive also posted a rebuttal on the Forgejo discussion regarding the shift of Gitea to Open Core.

# Forgejo moved from MIT licence to GPL. What was the thinking behind this? What additional benefits does this offer?


    It allows contributions from Free Software developers who only publish under a copyleft license
    It is an additional guarantee that Forgejo will not switch a non Free Software license in the future
    It establishes Forgejo as a copyleft product in a non-controversial way because it is the same kind of license as Git


# Did you consider the conceptually similar EUPL, which was specifically crafted to fit with well with the legal regimes of all EU member states and also takes the SAAS loophole into account ( given that Forgejo is typically going to be offered as a service)? Or FSF's Affero GPL?

Yes, and the transition to such a license is an ongoing discussion where the merits of the various licenses are debated, among other things. There is not yet a consensus and it is too early to know how or when it will be concluded.

# Will this new license asymmetry not hamper sharing development effort with Gitea? We notice plenty of backporting from Gitea to Forgejo is still taking place, is there an estimate of what percentage of Gitea development is captured? 

As of August 2024 it is close to 100%. The licence change has little to no bearing on sharing development efforts, as Gitea, instead of cherry picking from Forgejo, is rather reimplementing features, which can also be done after a licence change. Attempts to upstream Forgejo changes were met with hostility. Since MIT is GPL compatible, Forgejo is still able to cherry pick from Gitea.

We should also note that files bearing an SPDX license header that is MIT can still be taken as-is by other forks and upstreams - the license switch will mostly affect newly created files, unless if a developer decides to modify that said header by hand when modifying an existing file that also exists in Gitea.

Furthermore, we have observed that the focus on Gitea seems to be more on the development of its proprietary version, as indicated by the full history of releases: https://web.archive.org/web/20240902074615/https://gitea.com/commitgo/gitea-ee/activity/yearly

# Is there a policy regarding cherry-picking from Gitea’s development? 

Forgejo contributors are organized to examine all changes happening in Gitea on a weekly basis and cherry pick them. See https://codeberg.org/forgejo/forgejo/pulls?labels=85536 for a full inventory.

When we cherry pick from Gitea, we have relaxed our normal acceptance criteria in some respects (mostly with regards to tests), but what we cherry pick, is largely the burden of the Forgejo contributor doing the weekly cherry picking, and the reviewers.

We do try to carefully analyze what we pick - for a good reason, as we caught bugs (even security issues) during the weekly cherry pick.


# How do you manage this on an ongoing basis, are there e.g. softfork tools you use for this? Is this tooling integrated into Forgejo itself, so users can use it for synchronising other projects too?

It lives in a separate repository https://codeberg.org/forgejo/tools/src/branch/main/scripts/wcp It is heavily coupled with a methodology that fits the Forgejo / Gitea dynamic and there has been no discussions on how it could be generalized to other projects.

# Are you aware of the reverse happening (i.e. code developed in Forgejo to be added to Gitea), or are improvements in Forgejo ignored since the hard fork and/or the license change? 

Code developed in Forgejo has been added to Gitea on a number of occasions. But it was never done by cherry picking the Forgejo commits. It was always and systematically done by re-implementing the change, even when it was trivial. That has been the case since the inception of Forgejo two years ago, even when Forgejo was 100% MIT.

# Or do you even have an active policy of contribution yourself, to keep compatibility for as long as you can?

Gitea is actively and deliberately creating differences with Forgejo by re-implementing Forgejo changes instead of cherry picking them. Attempting to contribute changes to improve the compatibility between the two codebases in this context would be an uphill battle.

The best Forgejo can do to preserve some compatibility is to integrate Gitea changes that make it possible for a Gitea instance to migrate to a Forgejo instance.

# What are unique features of Forgejo?

There are a number of unique features in Forgejo and there is no exhaustive list. They relate to security (for instance Forgejo database are by default more resistant to brute force attacks of passwords or this week release to fix a failure to enforce a token scope), user interface (for instance the optional display of release assets) or backend (for instance manually running a CI job fixing the leak of multi-architecture container images).

Other unique features include: 

    Displaying the GPG/SSH signature of tags, similar to how commit signatures are displayed
    Soft-quota support (in v9)
    shields.io-based repo badges (Gitea has something similar, without shields.io, but also much more limited)
    Federated Star
    User activity following via ActivityPub (WIP PR)

We'd also highlight release compatibility: our v7 and v8 branches contain no breaking changes. We put a much higher emphasis on making it easier to deploy Forgejo, in a way that is easy and safe to upgrade within a major version. And as painless between major versions as possible, too.

# Would you still consider Gitea upstream, or have the codebases meanwhile diverged to such a point that this won't last long (note that Forgejo has more commits)?

At present Gitea is more of a Forgejo dependency than an upstream.

It is to be noted that should a new feature be merged in the Gitea codebase, it would not qualify as ready to be released by Forgejo standards unless they contain automated tests that demonstrate they actually work.

# Since you aim to cater for the whole range from large organisational and public instances (e.g. Codeberg) to small personal instances, how do you evaluate trade-offs - especially in the planned user experience improvements? Can you benefit from modularity for instance to make ‘single-party’ instances, or accommodate different workflows (like Fossil or Sourcehut)? (E.g. with less social and discovery features, perhaps a different organisation of the home page focusing on the project, etc.)

Before April 2024 there was no user centric approach that could be used to figure out how to manage the gap between the need of large scale instances and personal ones. The historic approach has been to add various settings and leave the admin with the daunting task of figuring out what they can do by themselves.

The effort started a few months ago to establish a new process to design features in Forgejo that followed the User Research efforts is meant to remedy this. It is too early to know what approach will be chosen but it can only be an improvement compared to what has been the default practice since the beginning.

# Is federation still on the roadmap? 

Yes. It is still and will always be the number one priority for Forgejo.

# What is the status and planning?

It got momentum in the recent months: User activity following via ActivityPub is under active development. This work may be ground for a request for payment in the context of the existing Forgejo federation grant 2022-08-067, if it is merged soon enough (it expires 1 December 2024).

But, in a nutshell, federation is not usable yet. There has been little activity until early 2024. It has been exceptionally challenging to find contributors (funded or volunteers) motivated to work on implementing federation features. Although everyone agrees there is no higher priority.

This grant application does not contain federation related tasks because there already is a grant covering that work and it is our understanding that it would be asking to fund the same topic twice.

# "Localization: owned since early 2024 it needs code adjustements and proper tooling to improve the translation quality and support for additional languages." - We didn't quite get the meaning, can you rephrase. Are (FOSS) localisation workflows like Weblate integrated into Forgejo, so that projects can easily add these to their projects (often, people have to have a separate workflow for this outside of their repository, which creates a sort of split universe)?

The localization core hasn't changed since Gogs, and has some root problems:

    .INI being tricky to parse by other tooling and having poor support as a translation format overall across various platforms
    .INI not supporting different amounts of string plural versions, which has to be in range 1-5. Currently we just support English-like plurals, limited to 2, which makes it impossible to make some texts read well
    Throughout the templates, we have to specify both plural versions everywhere where needed, which is noisy and essentially means hard-coding the 2 variations limit

Fixing all these issues is necessarily to have an actually good localization, but it requires a lot of refactoring work, involving writing tests, updating templates and converting the whole localization format eventually.
