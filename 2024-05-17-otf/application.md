# FOSS Sustainability Fund 2024

## General information

* **Project Name**: Forgejo (https://forgejo.org)
* **What is your name?**: Forgejo Contributors
* **What email address should we use to contact you?**: otf-2024@forgejo.org
* **How much funding do you estimate you will need?**: 400,000 USD
* **How long do you estimate this project will take?**: 24 months

## Problem Statement

> Provide a brief explanation of the sustainability problems your FOSS project is facing, and briefly mention the problems being addressed, at both technical and non-technical levels. Highlight which ones are critical and non-critical. (500 words)

Forgejo is a web-based collaborative software platform for hosting and collaborating on versioned-documents, similar to GitHub or GitLab. It supports many kind of source code, for instance to support computer application, websites, publication papers, artistic work, ACE (Architecture, Engineering, Construction). It is easy to deploy and lightweight, has an integrated CI (Continuous-Integration) and can be self-hosted on inexpensive hardware.

Its user base grew since 2019, when Codeberg was created. In 2024 Forgejo is used as a collaborative development platform by over 50,000 Free Software projects. There are over 100,000 registered Forgejo users and thousands of self-hosted instances.

In the past decades, and despite numerous attempts, software forges developed in the interest of the general public failed to become sustainable (phabricator, Allura, ...). Forgejo has reached a point where it needs a push to become a self-sustaining and durable project.

### Critical

* Roadmap - in the absence of a roadmap:
  - decision makers and founders cannot figure out if Forgejo is aligned with their own objectives.
  - users and admins cannot know if Forgejo is heading in a direction that matches their use case.
  - contributors waste energy figuring out where their effort would have the greater impact.
* User Research - UI and UX is implemented without observing users interactions which leads to usability and accessibility problems.
* Recruiting contributors - new users generate a workload that keeps growing.
* Security audit - the capacity of the security team does not allow for the audits required by HRD.
* Test - insufficient tooling & coverage
  - the test suite is difficult to use and makes contribution difficult.
  - large parts of the codebase still lack coverage.
* Funding - Forgejo is governed by a majority of volunteers who are estimated to also accomplish 80% of the work alongside a paid staff of three people. There is not enough funding to employ additional paid staff to keep that balance.
* Data portability - the code to migrate to and from other forge software only covers part of the data they contain.
* Documentation - content and tooling are missing
  - there is almost no tooling
  - the user and admin guides are only at 50% completion
* Bugs backlog - There are currently over 300 reported bugs and it keeps growing.
* Localization - As the Forgejo localization team create early 2024, the quality of over 5000 strings have not been verified.
* Scaling out - federation is not implemented and requires Forgejo to scale up instead of scaling out. It is orders of magnitude more costly and complex.

### Non-critical

* Technical debt - Fundamental parts of the codebase need refactoring.
* Accessibility - WCAG audits must be conducted.
* Packaging and distribution - Some architectures, operating systems, packages and distributions channels are still missing support.
* Moderation and well-being - Disruptive behavior needs to be dealt with in a timely manner.
* Promotion, adoption & integration - Software developers and potential users are not aware of Forgejo's existence.

## Sustainability

> How will this potential OTF support address these sustainability problems, during and beyond the period of funding? (500 words)

### Create a non-profit organization that is not in control of Forgejo

Forgejo is driven by a variety of independent actors:

* Codeberg, custodian of the domain - dedicated to operating a single instance hosting over 100,000 projects and developers
* Volunteers
* Employees delegations
* Independent contractors paid by their clients
* Individuals funded by grants

They strike a balance that is essential to the growth of Forgejo. The non-profit organization created to manage the grant and address Forgejo sustainability problems will:

* Pay invoice approved by the Forgejo community in accordance with the decision making process that is already in place.
* Have no control over the Forgejo domains.
* Be one among many actors working to further Forgejo.
* Operate transparently.

### Get to know Forgejo users and admins with User Research

With User Research, Forgejo will get to know who are the people and organizations using it. By observation and first hand testimonies, it will collect evidence that can be used to:

* Identify the most pressing issues
* Prioritize features
* Improve User eXperience

The recommendations of the User Research reports will define a user centric product. 

### Create a collective roadmap

Forgejo, being driven by a collective of loosely coupled actors, requires an unusual approach to building a roadmap. There is no telling when a given actor will disappear or show up to tackle a given topic. However, funding and User Research offer a great solution to ensure the continuity of the project:

- **User Research** observations define a limited set of topics that matter to Forgejo users.
- **Funding** ensures that the items on the roadmap have momentum, even if no independent actor is motivated to work on them.

The resulting collective roadmap will therefore be composed of:

- Items defined by User Research and advanced by funding if needs be.
- Items that volunteers decide to work on.

### Help grow the number of volunteers & paid staff

Forgejo contributors are already involved in each area with critical sustainability problems. They need to be grown while preserving a balance between volunteers and paid staff. The focus is on recruiting volunteers if they are not the majority or find paid staff if there are too little of them.

An important part of the job description of all paid staff is to help ensure volunteers feel appreciated and in control. They are and must remain the driving force behind every aspect of Forgejo.

### Define and implement a long term funding strategy

A software forge is a complex piece of software. Ensuring funding to pay for one person full time for each skillset will always require significant funding. Strategies must be defined collectively, in a way that is acceptable to all actors.

In the past, Forgejo was funded by:

- Grants from government bodies
- Anonymous donations from individuals or organizations
- Employee delegation

## Project

> Description: Provide a high-level overview of the proposed scope of work, objectives, activities, and deliverables. (500 words)

### Scope of work

* Maintenance
  - Usability and accessibility improvements with User Research
  - Roadmap
  - Security audit
  - Data portability
  - Bugs backlog
* Operations
  - Test
  - Documentation
  - Funding and long-term strategic planning
* Community
  - Recruiting Contributors
  - Localization

### Objectives

* Keep organizational cost (administrative, accounting, etc.) under 10%
* A non-profit organization governed by all current Forgejo stakeholders manages the grant and long term funding
* User Research is conducted every year and made available with reports and raw data
* Roadmaps are published
  - for the next 12 months and updated monthly
  - for long term objectives and updated yearly
* A long term funding strategy is published and updated yearly
* The long term funding strategy is implemented within a year
* The bug backlog is cleared within two years
* Security audits are
  - conducted every year
  - the most pressing recommendations are acted upon within a year
* The user and admin documentation are completed within a year and continuously updated
* Within two years tests are
  - Refactored
  - Cover at least 75% of the codebase
* Supported translations
  - are reviewed within two years
  - each supported language has a reviewer within a year

### Activities

Forgejo progress toward long term sustainability is measured monthly with:

- A monthly report similar to https://forgejo.org/tag/report/
- Additional verifiable quantitative metrics

The timeline for the first year is:

* Q1
  - Establish a legal entity to sign the grant and receive funds
  - Recruit part time organizational staff
  - Recruit User Research paid staff
  - Conduct the first round of User Research focused on sustainability issues
  - Define a 12 months roadmap based on the User Research report
* Q2
  - Recruit volunteers and paid staff to implement the roadmap (technical writer, fundraiser, developer, UX designer, etc.)
  - Implement the 12 months roadmap 
  - Conduct a security audit
  - Define a funding strategy
* Q3 & Q4
  - Implement the 12 months roadmap
  - Act on the recommendations of the security audit
  - Define a long term roadmap
  - Implement the funding strategy
  - Define and document recurring activities addressing sustainability issues (funding, security audits, documentation, etc.)
  - Review project progress and adapt the strategy accordingly

The timeline for the second year follows the guide published to define activities related to sustainability.

### Deliverables

* Pull requests
  - clearing the bug backlog
  - refactoring tests
  - improving documentation tooling
  - improving documentation
  - updating the teams with new contributors
  - increasing test coverage to 75%
  - increasing data portability
  - user research reports & data
  - implementing federation
* Monthly reports with quantitative and auditable measures related to:
  - active contributors (paid staff & volunteers)
  - localization
  - number of bugs fixed
  - test coverage
  - funding and expenses
  - new and updated documentation items
  - implementation and metrics for the funding strategy
* Quarterly releases (software and documentation)
* Patch releases including security fixes
* Documented funding strategy
* Yearly User Research reports
* 12 months & long term roadmap

## Contributors & Community

> Provide an overview of the project’s core contributors, volunteer contributors, core maintainers, and volunteer maintainers. (500 words)

Forgejo is a community of people who contribute in an inclusive environment. Every month all contributors are listed in a report that tells the story of how they collectively made progress: https://forgejo.org/tag/report/. The participation grew significantly in 2024 with over 100 contributors monthly.

There is no notion of a core team in Forgejo. Instead there are a number of teams https://codeberg.org/forgejo/governance/src/branch/main/TEAMS.md composed of people interested in contributing actively to a given topic. The most active members in each team are what could be considered core team members in other projects. Only they do not have special privileges, they are bound to the same decision making process as all other contributors (https://codeberg.org/forgejo/governance/src/branch/main/DECISION-MAKING.md).

- https://codeberg.org/fnetX
  - co-maintainer of the Codeberg.org Forgejo instance
  - conducts user research interviews to improve the UI & UX
  - recruits contributors across the board and to improve UI in particular
  - security liaison with the security team
  - bugs and features scrubbing
- https://codeberg.org/earl-warren
  - hardware infrastructure development and maintenance
  - release management tooling and publishing
  - CI implementation
  - moderation enforcement and reporting
  - security fixes
  - documentation
  - fundraising
- https://codeberg.org/0ko
  - improves Forgejo internationalization and translatability
  - fixes base English strings
  - maintains Forgejo localization project on Weblate
  - Russian translator
  - consults potential translators, provides help to translators
  - maintains localization related documentation
  - fixes bugs and renovates UI
- https://codeberg.org/oliverpool - https://code.pfad.fr/
  - fixes bugs
  - interoperability improvements (webhooks)
  - large refactors
  - improvements to the test tooling
  - moderation and well being
  - fundraising
- https://codeberg.org/algernon
  - new features
  - bug fixes
  - improvements to the test tooling
  - dependency management tooling
- https://codeberg.org/viceice - https://github.com/viceice
  - dependency management and tooling
  - packages API & implementation
  - helm chart
- https://codeberg.org/caesar
  - Forgejo website design and implementation
  - documentation integration
  - UI & UX design
  - accessibility
- https://codeberg.org/jerger
  - implementation of federation features
- https://codeberg.org/patdyn
  - implementation of federation features

## Users & Beneficiaries

> Provide an overview of the project’s user base, their demographics and statistics, and any other indirect beneficiaries. (1000 words)

### Direct Beneficiaries

- Software developers assisting HRD
  - can work together using cheap hardware and Forgejo as a lightweight web collaborative platform in a secure network
  - it is the workbench where they create or modify software to evade surveillance in oppressive regimes and combat censorship using techniques specific to their regional context
  - it allows them to run tests, create and distribute quality releases to reduce the risk of security vulnerabilities when they are used in the field
  - its ease of deployment allows for creative ways to prevent surveillance, for instance a local deployment not connected to the Internet, tor hidden service or in a country where laws are less of a threat to privacy.
- Software developers - work together on a software writing code, testing, packaging and deploying.
- System administrators - install, upgrade and address security issues for the benefit the the users.
- Software users - download packaged software, report bugs and discuss new features.
- Service providers - bridge the gap between software users with no technical expertise and the software developers.

### Indirect beneficiaries

- Human right defenders depending on software developed, distributed and maintained on Forgejo instances only accessed via tor, by way of making solid, modifiable, accessible tools more readily available and promoting technical independence
- Users of a software developed and maintained on Forgejo
- The general public, by promoting independent community exchange and volunteering approaches

### Demographics

- Most users are drawn to Forgejo for ethical reasons
- World-wide with a majority based in Europe
- Most developers who participate maintain or contribute to a Free Software codebase
- Most system administrators are volunteers
- Service providers are small companies advertising Free Software expertise

See also the User Research repository https://codeberg.org/forgejo/user-research

### Statistics

- Software projects depending on Forgejo - over 50,000 mainly on Codeberg.org and other instances listed at https://codeberg.org/forgejo-contrib/delightful-forgejo#public-instances
- Releases - at least one major release every six months and one patch release every month https://forgejo.org/releases/
- Contributions - hundreds of messages exchanged daily, dozens of contributors monthly, over a hundred participants monthly as described in the monthly report https://forgejo.org/tag/report/
- Security - regular security release https://forgejo.org/tag/security/ https://codeberg.org/forgejo/security-announcements/issues
- Software developers and users - over 100,000 on https://codeberg.org and [other public instances](https://codeberg.org/forgejo-contrib/delightful-forgejo#public-instances)
- System administrators and service providers - thousands based on the number of:
  - OCI images downloads ([v7](https://codeberg.org/forgejo/-/packages/container/forgejo/7) ~4,000, [v1.21](https://codeberg.org/forgejo/-/packages/container/forgejo/1.21) ~80,000
  - [Binary downloads](https://codeberg.org/forgejo/forgejo/releases) v7 ~1,000, v1.21 ~5,000

The further exploration and expansion of the user and beneficiary base of Forgejo constitutes one of the principal aims of the proposed project, as well as increasing adoptability for individuals and organizations beyond those already involved. As such, the proposed efforts directly contribute to further integration of Free Software engagement in general.

## Dependencies

> Provide an overview of the project’s software upstream and downstream dependencies. (500 words)

### Upstream

All upstream dependencies are exclusively Free Software and the development lifecycle does not depend on any third party proprietary services. Upstream dependencies are managed with dedicated tooling and Forgejo contributors care for them on a regular basis.

- Development - all tools and services used for Forgejo development are considered upstream dependencies of Forgejo because its development cannot move forward without them.
  - Forgejo is the collaborative platform used by Forgejo developers https://codeberg.org/forgejo/forgejo
  - Localization uses Weblate https://translate.codeberg.org/projects/forgejo/
  - Forgejo has its own Continuous Integration software called Forgejo Actions and an ecosystem of plugins similar to GitHub Actions, all of which are verified to be Free Software
- Distribution - binaries, OCI images and other release artifacts maintained by Forgejo are distributed on a Forgejo instance instead of a third party service such as the Docker Hub
  - OCI images downloads https://codeberg.org/forgejo/-/packages/container/forgejo/versions
  - Binary downloads https://codeberg.org/forgejo/forgejo/releases
- Runtime - Forgejo is a standalone binary that includes all its Go and JavaScript dependencies
  - Go https://codeberg.org/forgejo/forgejo/src/branch/forgejo/go.mod
    - infrastructure (gitea, chi, cors, swagger, gocron, ...)
    - communications (ssh, grpc, protobuf, activitypub, ldap, httpsig, otp, imap, RSS ...)
    - authentication (webauthn, goth, go-crypto, pam, OpenID ...)
    - database (MySQL, PostgreSQL, SQLite, ...)
    - search (meilisearch, bleve, elasticsearch)
    - VCS (git)
    - Object storage (minio, garage)
    - UI components (go-enry, editorconfig, goldmark, ...)
  - JavaScript https://codeberg.org/forgejo/forgejo/src/branch/forgejo/package.json
    - frameworks (vue, tailwind, ...)
    - modules (cssloader, postcss, ...)
    - UI components (asciinema-player, mermaid, pdfobject, ...)

### Downstream

- Packages are maintained for various distributions channels https://codeberg.org/forgejo-contrib/delightful-forgejo#packaging (YunoHost, Alpine, Arch, Debian GNU/Linux, NixOS, Snapcraft, ...)
- Over 50,000 Free Software projects use Forgejo in their development lifecycle, for collaboration and distribution. Forgejo can be considered an integral part of these software rather than a third party service.
  - On self-hosted instances
  - On Codeberg https://codeberg.org/explore/repos?sort=moststars
  - On other instances with public registration https://codeberg.org/forgejo-contrib/delightful-forgejo#public-instances

## Licensing

> Provide an overview of the project’s open source licensing. (500 words)

The [license of Forgejo](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/LICENSE) is [MIT](https://spdx.org/licenses/MIT.html).

The licenses of the dependencies that are compiled in Forgejo are collected, embedded in the binary and displayed at https://code.forgejo.org/assets/licenses.txt.

## Team Expertise & Organizational Capacity

> Why are you, and your team members, the right people to maintain this project? (500 words)

The team members supporting this grant are the contributors listed in the "Contributors & Community" section all have a history of sustained and significant contributions to Forgejo.

Together, they cover all aspects in critical need of attention to improve Forgejo's sustainability. They are in the best possible position to ensure funding to improve Forgejo sustainability is spent wisely in their respective areas of expertise.

Their respective involvement in the Forgejo project already reflects the desired balance between volunteers and paid staff. All of them volunteer in one way or another, even when they are paid during a given period of time. Some of them may be willing to be paid for their work during the execution of the grant. Even if they don't, they will act in an advisory capacity to review the work done by paid staff.

* Never derived any income from Forgejo
  - https://codeberg.org/0ko
  - https://codeberg.org/earl-warren
  - https://codeberg.org/caesar
  - https://codeberg.org/jerger
  - https://codeberg.org/patdyn
* Delegated part time to Forgejo by their employer
  - https://codeberg.org/viceice
* Have been paid for their work on Forgejo
  - https://codeberg.org/algernon
  - https://codeberg.org/oliverpool
  - https://codeberg.org/fnetX

Forgejo is not incorporated and has no organizational capacity in the traditional sense. However it has an established governance rooted in a well defined decision making process. It proved to be effective for simple matters such as accepting a new team member or important ones such as licensing. The organization created to manage this grant and future funding will be governed by the same decision process.

One person with no past history in the Forgejo project will need to be recruited part time to care for the administrative aspects associated with the incorporation of the organization and day to day duties.
