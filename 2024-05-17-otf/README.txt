Received 28 June 2024

---

Determination: Dismissed

 We very much appreciate your submission to Open Technology Fund (OTF) FOSS Sustainability Fund for consideration. Upon evaluation, we have decided that OTF is not able to provide financial support for your project at this time.

At the end of this message we have provided feedback from our determination. If you have any questions or comments, please reply to this email with your input. We very much welcome a continued discussion. 

We also encourage you to visit our applicant guidebook: https://docs.opentech.fund/otf-application-guidebook/our-funds-and-fellowships/free-and-open-source-software-foss-sustainability-fund. There we list considerations in our evaluation process. 

Check out News, Updates, and Announcements (https://www.opentech.fund/news/) from OTF to learn about the latest developments on our programs, press, and research.

Again, we cannot express enough our appreciation for your submission and your patience while we completed our evaluation process.

---

During this round of the FOSS Sustainability Fund, OTF has received an unexpectedly high number of competitive, quality applications from FOSS projects, and is prioritizing funding strongly-mission aligned privacy and censorship circumvention projects. While your FOSS technology is important for the public interest, and we understand the importance of its sustainability, it does not fall within the specific priority areas of funding and mission alignment for this round. OTF’s overall mission is to foster development and ensure sustainability of technologies that circumvent repressive censorship and surveillance or increase communication access and safety.
