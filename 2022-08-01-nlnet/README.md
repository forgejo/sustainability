## Summary

The funds made available by this grant were available for implementing the tasks found in the MoU from 1 December 2022 to 1 December 2024. Payment was sent for part of the moderation work (2,500€). The remaining funds (47,500€) were not claimed and will be used at NLnet discretion for other projects.

* Grant identifier: 2022-08-067
* URL: https://nlnet.nl/project/Federated-Forgejo/

## Tasks and pull requests

* Pull requests completed and the milestones they match
  * https://codeberg.org/forgejo/forgejo/pulls/540
    * Task 3 Moderation tools (25%)
	  * Blocking a user
	  * Moderation UI
  * https://code.forgejo.org/forgejo/end-to-end/pulls/195 & https://code.forgejo.org/forgejo/end-to-end/pulls/196 & https://codeberg.org/forgejo/forgejo/pulls/1680 & https://codeberg.org/forgejo/forgejo/pulls/3494 & https://codeberg.org/forgejo/forgejo/pulls/3792
	* Task 4 Testing tools and code for federation (20% @earl-warren, 5% @patdyn/@jerger/@clemensg)
	  * Two Forgejo instances communicate via ActivityPub to star a repository
* Pull requests in flight and the milestones they match
  * https://codeberg.org/forgejo/forgejo/pulls/4767 & https://codeberg.org/forgejo/forgejo/pulls/3494 & https://codeberg.org/forgejo/forgejo/pulls/3662 & https://codeberg.org/forgejo/forgejo/pulls/3792
	* Task 1.1 Actors (75%)
	  * Send out activities to followers
	  * Render followed user updates in your user feed
	* Task 1.3 Sending/receiving activities (100% @patdyn/@jerger/@clemensg)
	* Task 1.4 Database representation of remote data (5% @algernon, 20% patdyn/@jerger/@clemensg)
	  * Remote owner/user
	* Task 1.5 Serving/fetching ActivityStreams (50%)
	  * Generate ActivityStreams representation of comments
	  * Render remote owners/users in the UI
	* Task 1.6 Remote interactions (50%)
	  * Federated owner/user
  * https://codeberg.org/forgejo/sustainability/pulls/50#issuecomment-2183365
	* Task 1.1 Actors (25%)
	  * Periodically fetch and update cached remote users' data
	  * Clean out avatar cache
* Service requests
  * Forgejo Runner Security Audit https://codeberg.org/forgejo/discussions/issues/204
