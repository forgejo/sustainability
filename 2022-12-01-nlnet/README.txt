application.txt was submitted November 25th, 2022
https://codeberg.org/forgejo/funding/issues/1#issuecomment-694807
Code: 2022-12-035

Codeberg tasks were completed in full as per the RfP in this
directory, between the beginning of the grant and March 2024.

The grant will expire on 31 December 2024 (an extended period negotiated with NLnet).
negotiated privately.

A 10K€ extension budget to the grant is informally agreed on by NLnet but is yet to be formalized as of September 2024.

### regarding the extension

#### 2024-07-04 from NLNet:

>> Given your readiness to accommodate us by adding an extra task for the webhook work in order to allow both that and the UI work to be completed, we wondered if you would also be willing to extend the timeline of the MoU in order to allow the UI tasks to be completed?
>
> We can indeed give this a few month extra. We’d like to ensure the grant is completed by the end of this year, as the whole NGI0 Entrust programme comes to a halt next year.

#### 2024-07-07 from NLNet:
>> Thanks for the reply. Yes, an extension of a few months would be very helpful to allow the UI tasks to be completed. If it can be extended to the end of the year that would be ideal. What do you need from our side to make this happen?
>
> OK, let’s aim to complete by the end of the year; let us know in case you will have further obstructions delaying the effort. No further action needed; our goal is that you can just focus on the code. ;)

### regarding the extra tasks

#### 2024-07-04 from NLNet:

>> Regarding the possible extra tasks, we submitted an application to NLNet NGI0 Commons (grant application 2024-04-364) to improve the testing situation. Maybe a couple of tasks could be instead completed within the current grant. How much budget could we expect?
>
> We can indeed unblock you by including some such newly proposed work in the current grant, no problem. We can stretch the budget by at most €10k, feel free to make a suggestion including tasks you proposed in the previous email and/or improving the testing situation.

#### 2024-07-07 from NLNet:

>> We’ll get back to you soon with a proposal for the additional work to be added.
>
> Great, there is no rush from our side; but of course it may be comforting on your side to know the tasks and rewards are agreed upon before you spend your time on them.
