B1. Tag based release configuration

Results/Deliverables:

* Running the tests
  * Workflow https://codeberg.org/forgejo/forgejo/src/commit/1684f0e5bf0b7317c2025e15f33a8d6eca65d503/.forgejo/workflows/testing.yml
  * Unit tests https://codeberg.org/forgejo/forgejo/src/commit/1684f0e5bf0b7317c2025e15f33a8d6eca65d503/.forgejo/workflows/testing.yml#L75
  * Integration tests
    * https://codeberg.org/forgejo/forgejo/src/commit/1684f0e5bf0b7317c2025e15f33a8d6eca65d503/.forgejo/workflows/testing.yml#L121
    * https://codeberg.org/forgejo/forgejo/src/commit/1684f0e5bf0b7317c2025e15f33a8d6eca65d503/.forgejo/workflows/testing.yml#L168
    * https://codeberg.org/forgejo/forgejo/src/commit/1684f0e5bf0b7317c2025e15f33a8d6eca65d503/.forgejo/workflows/testing.yml#L205
  * Tests for known security vulnerabilities have been included in both integration & unit tests
* Integration build
  * Workflow https://codeberg.org/forgejo/forgejo/src/commit/1684f0e5bf0b7317c2025e15f33a8d6eca65d503/.forgejo/workflows/build-release.yml
  * Action to build a release https://code.forgejo.org/forgejo/forgejo-build-publish/src/branch/main/build
* Chaining the build to publish
  * Workflow https://codeberg.org/forgejo/forgejo/src/commit/1684f0e5bf0b7317c2025e15f33a8d6eca65d503/.forgejo/workflows/build-release.yml#L210-L230
* Experimental & stable publication workflow
  * Workflow https://codeberg.org/forgejo/forgejo/src/commit/1684f0e5bf0b7317c2025e15f33a8d6eca65d503/.forgejo/workflows/publish-release.yml
  * Action to publish a release https://code.forgejo.org/forgejo/forgejo-build-publish/src/branch/main/publish
* Release process documentation
  * Stable releases https://forgejo.org/docs/v1.21/developer/release/#stable-release-process
  * Experimental releases https://forgejo.org/docs/v1.21/developer/release/#experimental-release-process

Remarks:

The documentation explaining the release process based on these tools can be found at https://forgejo.org/docs/v1.21/developer/release/

The build & publish actions have been refactored and bundled into a single repository to ease development and testing.

C1. LXC helpers

Results/Deliverables:

* Helper to create & destroy a container
  * https://code.forgejo.org/forgejo/lxc-helpers/src/commit/231215c11d38df793521766dcce0e80e824614ca/README.md
* Integration tests to verify each helper
  * Workflow https://code.forgejo.org/forgejo/lxc-helpers/src/branch/main/.forgejo/workflows/test.yml
  * Tests
    * https://code.forgejo.org/forgejo/lxc-helpers/src/commit/231215c11d38df793521766dcce0e80e824614ca/tests.sh
    * https://code.forgejo.org/forgejo/lxc-helpers/src/commit/231215c11d38df793521766dcce0e80e824614ca/lxc-helpers-lib-test.sh
* Performance improvements
  * LXC templates are re-used and layered https://code.forgejo.org/forgejo/lxc-helpers/src/commit/231215c11d38df793521766dcce0e80e824614ca/lxc-helpers-lib.sh#L328-L343
* Tagged release of the helpers
  * Workflow to inject the helpers into its dependencies that are tagged
    * https://code.forgejo.org/forgejo/lxc-helpers/src/commit/231215c11d38df793521766dcce0e80e824614ca/.forgejo/workflows/cascade-act.yml
    * https://code.forgejo.org/forgejo/lxc-helpers/src/commit/231215c11d38df793521766dcce0e80e824614ca/.forgejo/cascading-pr-act

Remarks:

The LXC helpers are now also used to deploy and maintain all the Forgejo hardware infrastructure.

* https://forgejo.org/docs/v1.21/developer/infrastructure/#containers

C3. LXC backend

Results/Deliverables:

* Release that includes the LXC backend
  * https://code.forgejo.org/forgejo/act/releases/tag/v1.20.1
  * https://code.forgejo.org/forgejo/act/src/tag/v1.20.1/pkg/runner/lxc-helpers.sh
  * https://code.forgejo.org/forgejo/act/src/tag/v1.20.1/pkg/runner/lxc-helpers-lib.sh
* Support for nested hardware virtualization (KVM) / system containers
  * Documentation https://forgejo.org/docs/v1.21/admin/actions/#execution-of-the-workflows
  * Pull Request https://code.forgejo.org/forgejo/act/pulls/22/files

Remarks:

The Forgejo helm chart CI contains end to end tests that rely on this LXC backend to
run a nested kubernetes cluster and verify the helm chart works.

https://codeberg.org/forgejo-contrib/forgejo-helm/src/commit/bb9cce5846a6bb18323be18422c57cfe8136e1cd/.forgejo/workflows/build.yml#L77

The LXC helpers integration tests themselves depend on the LXC backend
integration in the Forgejo runner to verify it can run nested KVM & K8s & LXC.
This was an interesting chicken & egg  problem to solve.

https://code.forgejo.org/forgejo/lxc-helpers/src/branch/main/.forgejo/workflows/test.yml#L6-L13
