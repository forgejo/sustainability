# Amendment of the workplan

The following tasks have replaced the remaining budget (€ 11k) from the initial workplan,
also see https://codeberg.org/forgejo/sustainability/issues/77 for details.

Beneficiaries:

- Codeberg e.V.
- @lenikadali
- @floss4good
- @Kwonunn

## Task E. Documentation update

Total: € 4 200

Milestones:

1. Forgejo itself receives a way to add small documentation snippets in-app, reusing the current translation system. They can be used for supplementing small information to menus without requiring a round-trip to the external documentation. (€ 1 200)
2. The documentation view in the website receives navigation menus to conveniently navigate between related pages. (€ 480)
3. The documentation about Forgejo Actions is split into smaller pages that are easier to read. The differences between Forgejo Actions and GitHub Actions are explained. (€ 1 080)
4. The config cheat sheet and example configuration are automatically generated from the code to reduce inconsistencies and manual effort. (€ 1 440)

## Task F. Moderation features: Reporting

Total: € 6 800

Milestones:

1. A reporting feature is implemented in the database. It ensures that content remains available for review, even if a user deletes it after a report was sent. (€ 1 200)
2. Reports from multiple users are combined in the database and don't create additional reports. (€ 1 200)
3. A configurable timeout automatically removes resolved reports. (€ 600)
4. Users can report the most relevant content types (at least: issue comments, repositories, users) (€ 1 200)
5. Forgejo admins can see a list of reports (€ 1 200)
6. Forgejo admins can perform common actions on the listed reports (content deletion, locking of user account) (€ 1 400)
