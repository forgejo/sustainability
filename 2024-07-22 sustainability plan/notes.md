# Grow number of contributors 

* keep 75/25 balance volunteer/paid 
    * through outreach 
* social growth: attract non technical contributors 
* current contributor input - what do you need? 

# User research 

* improve accessibility  
* communicate with UX/UI team 

# Funding 

* needed for contractors on
    * development 
    * organizational 
    * UX/UI 
    * technical costs 
    * other 

# Technical aspects

* security audit
* testing 
* data portability 
* bugs backlog
* localization 
* technical debt 

# Outreach and adoption  

* documentation incomplete/inaccessible 
* promote adoption 