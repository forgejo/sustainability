# Sustainability

Forgejo depends on resources to keep going in a durable way and the most precious is the time people spend working on it. Most volunteers may not be inclined to account for the number of hours they spent working on organizing a videoconference, discussing a particular governance issue or implementing a feature. But those who are provide a valuable insight regarding the durability of Forgejo: when cumulated with the time spent by paid staff working alongside them, it is good approximation of what it takes for Forgejo to be sustainable.

For instance when one person is paid to spend four weeks working on a feature alongside a volunteer, that's at least 300 hours of work total, probably more if they are passionate and work long hours. In 2022 the salary of the person who is paid will vary between 5,000€ and 10,000€ depending on the country they live in and their level of expertise. And while volunteers are, by definition, not paid for their contribution it does not mean their time is less valuable.

All other costs running Forgejo are comparatively much lower, just like any other Free Software project. The hosting costs rely on donations to Codeberg e.V. which keep Codeberg.org running. Donations to Forgejo could cover everything else: hardware, travel costs etc.

# Forgejo resources per year

This is a manually maintained record of the resources dedicated to Forgejo updated at least once a year.

## 2025

### Hardware

* 250€ (January) [Hetzner hardware](https://code.forgejo.org/infrastructure/documentation/src/commit/2ede9bf6c2e7dae07f9cd564c24fe28f649b3605/README.md#hetzner01), paid for by https://codeberg.org/viceice - https://liberapay.com/viceice/
* 4,200€ (350€ per month) KVM virtual machine 16 threads, 64GB RAM, 1TB DRBD volume backed by SSD, 1 IPv4 provided at no cost by https://www.octopuce.fr

## 2024

### Grants

* 21,500€ from Codeberg e.V. of which 4630€ are left as of 2024-09-19.
  * sources:
    * 20,000€ from NLNet grants in 2023
    * ~430€ explicitly dedicated to Forgejo in 2023, part of it was donated by [disroot](https://disroot.org/annual_reports/AnnualReport2023.pdf)
    * ~1,070€ added from Codeberg's general funding
  * expenses:
    * employee delegation (see below)
* 35,000€ from [NLNet 2022-12-035](https://codeberg.org/forgejo/sustainability/src/branch/main/2022-12-01-nlnet)
  * 30,000€ to https://codeberg.org
  * 5,000€ to https://codeberg.org/oliverpool

### Volunteer

* ~1400 hours (about 30h a week on average between January to December inclusive) https://codeberg.org/earl-warren https://codeberg.org/earl-warren?tab=activity

### Employee delegation

* 5,600€ (total of 20 days at the rate of 280€ per day) https://codeberg.org/dachary/ paid by https://easter-eggs.com
* https://codeberg.org/algernon ([worklog](https://codeberg.org/algernon/codeberg-worklog/src/branch/main/worklog.md))
  * 166 hours under contract by Codeberg e.V.
  * Implementation of [forgejo#1316](https://codeberg.org/forgejo/forgejo/issues/1316) sponsored by [@glts](https://codeberg.org/glts).

### Hardware

* 250€ (December) [Hetzner hardware](https://code.forgejo.org/infrastructure/documentation/src/commit/2ede9bf6c2e7dae07f9cd564c24fe28f649b3605/README.md#hetzner01), paid for by https://codeberg.org/viceice - https://liberapay.com/viceice/
* 4,200€ (350€ per month) KVM virtual machine 16 threads, 64GB RAM, 1TB DRBD volume backed by SSD, 1 IPv4 provided at no cost by https://www.octopuce.fr
* around 2,500€ (January to November included) Hetzner hardware hosting for all hetzner*.forgejo.org machines, paid for by https://codeberg.org/earl-warren/

## 2023

### Grants

* 3,600€ [to support Forgejo](https://codeberg.org/forgejo/sustainability/issues/9#issuecomment-956394) allocated by Codeberg e.V., of which only € 1260 were claimed (see Employee delegation)
* 50,000€ [to work on the release process and the codebase](https://codeberg.org/forgejo/sustainability/issues/1#issuecomment-855819) for the benefit of Codeberg e.V., https://codeberg.org/oliverpool, https://codeberg.org/crystal, https://codeberg.org/caesar. This project was funded through the [NGI0 Entrust Fund](https://nlnet.nl/entrust), a fund established by [NLnet](https://nlnet.nl/) with financial support from the European Commission's [Next Generation Internet programme](https://ngi.eu/), under the aegis of DG Communications Networks, Content and Technology under grant agreement No 101069594.

### Hardware

* 640.80€ (total of 106.80 per month from July to December inclusive) physical machine 32 threads, 64GB RAM, 2TB SSD, 1 IPv4 hosted at https://www.hetzner.com/ paid for by https://codeberg.org/dachary/
* 3,150€ (total of 350€ per month from April to December inclusive) KVM virtual machine 16 threads, 64GB RAM, 1TB DRBD volume backed by SSD, 1 IPv4 provided at no cost by https://www.octopuce.fr
* 200€ (total of 100€ per month from November to December inclusive) [hetzner{02,03}](https://forgejo.org/docs/v1.21/developer/infrastructure/#hetzner0203)
* 600€ (total of 100€ per month from July to December inclusive) [hetzner01](https://forgejo.org/docs/v1.21/developer/infrastructure/#hetzner01)

### Volunteer

* ~1400 hours (about 30h a week on average between January to December inclusive) https://codeberg.org/earl-warren https://codeberg.org/earl-warren?tab=activity
* No time tracking. https://codeberg.org/gusted https://codeberg.org/gusted?tab=activity

### Employee delegation

* 28,000€ (total of 100 days at the rate of 280€ per day) https://codeberg.org/dachary/ paid by https://easter-eggs.com
* 400h https://codeberg.org/org/meissa/members paid by https://meissa.de/
* 107 hours https://codeberg.org/algernon ([worklog](https://codeberg.org/algernon/codeberg-worklog/src/branch/main/worklog.md#2023-12)), under contract by Codeberg e.V.

## 2022

### Donations

* 380€ donated to Codeberg and earmarked for Forgejo.

### Grants

* 50,000€ [to further forge federation](https://forum.forgefriends.org/t/nlnet-grant-application-for-federation-in-gitea-deadline-august-1st-2022/823) for the benefit of https://codeberg.org/xy, https://codeberg.org/6543, https://codeberg.org/realaravinth, https://codeberg.org/gusted, https://codeberg.org/dachary paid by https://nlnet.nl/
    * 2,500€ was spent in 2023 to implement [self-moderation](https://codeberg.org/forgejo/forgejo/pulls/540) by https://codeberg.org/gusted.

### Volunteer

* ~24 hours https://codeberg.org/earl-warren https://codeberg.org/earl-warren?tab=activity
* No time tracking. https://codeberg.org/gusted https://codeberg.org/gusted?tab=activity 

### Employee delegation

* 12,600€ (45 days between October and December inclusive at the rate of 280€ per day) https://codeberg.org/dachary/ paid by https://easter-eggs.com

### Donations

* 7.98 € forgejo.org domain name & mail services registration paid for by https://codeberg.org/oliverpool
* 9.25 € forgejo.com domain name registration paid for by https://codeberg.org/caesar
* 100€ static web hosting https://codeberg.org/forgejo/website/issues/25 paid for by https://codeberg.org/dachary

### In kind

* https://codeberg.org/forgejo hosting and CI by Codeberg e.V.
